package org.enoy.fx.control;

import java.io.IOException;

public interface SimpleFXMLView {

	default void loadFxml(String fxml){
		try {
			RootFxmlLoader.load(this, null, fxml);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
}
