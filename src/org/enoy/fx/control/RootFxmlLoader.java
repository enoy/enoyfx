package org.enoy.fx.control;

import java.io.IOException;
import java.io.InputStream;
import java.util.ResourceBundle;

import javafx.fxml.FXMLLoader;

public class RootFxmlLoader
{

	public static void load(Object root, Object controller, ResourceBundle bundle, InputStream inputStream) throws IOException
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setRoot(root);
		loader.setController(controller);
		loader.setResources(bundle);
		loader.load(inputStream);
	}

	public static void load(Object rootController, ResourceBundle bundle, InputStream inputStream) throws IOException
	{
		load(rootController, rootController, bundle, inputStream);
	}

	public static void load(Object rootController, ResourceBundle bundle, String resourceNameFromRootControllerClass)
		throws IOException
	{
		load(rootController, bundle, rootController.getClass().getResourceAsStream(resourceNameFromRootControllerClass));
	}

}
