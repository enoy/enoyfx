/**
 * 
 */
package org.enoy.fx.control.controlsfx;

import org.controlsfx.control.PropertySheet.Item;
import org.controlsfx.property.editor.PropertyEditor;

import javafx.scene.Node;

/**
 * @author Enis.Oezsoy
 */
public class CustomPropertyEditor<T> implements PropertyEditor<T>
{

	protected Item item;
	protected Node node;

	public CustomPropertyEditor(Item item)
	{
		this.item = item;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getValue()
	{
		return (T) item.getValue();
	}

	@Override
	public void setValue(T value)
	{
		item.setValue(value);
	}

	@Override
	public Node getEditor()
	{
		return node;
	}

}
