/**
 * 
 */
package org.enoy.fx.control.controlsfx;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.controlsfx.property.editor.PropertyEditor;

/**
 * @author Enis.Oezsoy
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomPropertyEditorClass
{
	public Class<? extends PropertyEditor<?>> value();
}
