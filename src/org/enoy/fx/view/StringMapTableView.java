/**
 * 
 */
package org.enoy.fx.view;

import javafx.collections.FXCollections;
import javafx.util.converter.DefaultStringConverter;

/**
 * @author Enis.Oezsoy
 */
public class StringMapTableView extends MapTableView<String, String>
{

	public StringMapTableView()
	{
		super(FXCollections.observableHashMap(), new DefaultStringConverter(), new DefaultStringConverter());
	}

}
